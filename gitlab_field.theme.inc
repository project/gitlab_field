<?php

/**
 * @file
 * Theme functions for GitLab Field feature.
 */

/**
 * Theme function for GitLab file tables.
 *
 * @see _gitlab_field_widget_get_linked_files()
 */
function theme_gitlab_field_widget_file_table($variables) {
  $element = $variables['element'];

  $table = array(
    '#theme' => 'table',
    '#header' => $element['#header'],
    '#rows' => array(),
    '#attributes' => empty($element['#attributes']) ? array() : $element['#attributes'],
    '#attached' => empty($element['#attached']) ? array() : $element['#attached'],
    '#caption' => $element['#caption'],
    '#colgroups' => $element['#colgroups'],
    '#sticky' => $element['#sticky'],
    '#empty' => $element['#empty'],
  );

  $rows = $element['rows'];

  foreach (element_children($rows) as $row_index) {
    $row = array();
    foreach (element_children($rows[$row_index]) as $row_prop) {
      if ($row_prop == 'data') {
        $row[$row_prop] = array();
        foreach (element_children($rows[$row_index][$row_prop]) as $col_index) {
          $col_children = element_children($rows[$row_index][$row_prop][$col_index]);
          if (empty($col_children)) {
            $row[$row_prop][$col_index] = drupal_render($rows[$row_index][$row_prop][$col_index]);
          }
          else {
            foreach ($col_children as $col_prop) {
              if ($col_prop == 'data') {
                $row[$row_prop][$col_index][$col_prop] = drupal_render($rows[$row_index][$row_prop][$col_index][$col_prop]);
              }
              else {
                $row[$row_prop][$col_index][$col_prop] = $rows[$row_index][$row_prop][$col_index][$col_prop]['#markup'];
              }
            }
          }
        }
      }
      else {
        $row[$row_prop] = $rows[$row_index][$row_prop]['#markup'];
      }
    }
    $table['#rows'][] = $row;
  }

  return drupal_render($table);
}

/**
 * Theme function for linked files tables.
 *
 * @see _gitlab_field_widget_get_linked_files()
 */
function theme_gitlab_field_widget_linked_files_table($variables) {
  $element = $variables['element'];

  $table = array(
    '#theme' => 'table',
    '#header' => $element['#header'],
    '#rows' => array(),
    '#attributes' => $element['#attributes'],
    '#attached' => $element['#attached'],
    '#caption' => $element['#caption'],
    '#colgroups' => $element['#colgroups'],
    '#sticky' => $element['#sticky'],
    '#empty' => $element['#empty'],
  );

  foreach (element_children($element) as $item_index) {
    $row = array('data' => array(), 'class' => array('draggable'));
    foreach (element_children($element[$item_index]) as $col_name) {
      // Skip file id.
      if ($col_name == 'fid') {
        continue;
      }
      $row['data'][] = drupal_render($element[$item_index][$col_name]);
    }
    $table['#rows'][] = $row;
  }

  return drupal_render($table);
}

/**
 * Theme function for the file browser's breadcrumb.
 *
 * @see _gitlab_field_widget_get_file_browser()
 */
function theme_gitlab_field_widget_file_browser_breadcrumb($variables) {
  $element = $variables['element'];
  $buttons = $element['buttons'];

  $links = array();
  foreach (element_children($buttons) as $key) {
    $links[] = render($buttons[$key]);
  }

  return '<p>' . implode(' / ', $links) . '</p>';
}

/**
 * Theme function for widget messages.
 *
 * This is a modification of the built in theme_status_messages() function.
 *
 * @see gitlab_field_field_widget_form()
 */
function theme_gitlab_field_widget_messages($variables) {
  $messages = $variables['messages'];
  $type = $variables['type'];

  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );

  $output .= "<div class=\"messages $type\">\n";
  if (!empty($status_heading[$type])) {
    $output .= '<h2 class="element-invisible">' . $status_heading[$type] . '</h2>' . PHP_EOL;
  }
  if (count($messages) > 1) {
    $output .= '<ul>' . PHP_EOL;
    foreach ($messages as $message) {
      $output .= '<li>' . $message . '</li>' . PHP_EOL;
    }
    $output .= '</ul>' . PHP_EOL;
  }
  else {
    $output .= reset($messages);
  }
  $output .= '</div>' . PHP_EOL;

  return $output;
}
