<?php

/**
 * @file
 * User related hook implementations for GitLab Field feature.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function gitlab_field_form_user_register_form_alter(&$form, &$form_state, $form_id) {
  _gitlab_field_form_user_register_profile_form_alter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function gitlab_field_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  _gitlab_field_form_user_register_profile_form_alter($form, $form_state);
}

/**
 * Helper function, which alters the user register/profile forms.
 */
function _gitlab_field_form_user_register_profile_form_alter(&$form, &$form_state) {
  module_load_include('inc', 'gitlab_field', 'gitlab_field.field');
  // Load the GitLab field defaults for this user.
  $defaults = _gitlab_field_user_defaults_load($form['#user']->uid);
  // Add the default selection widget.
  _gitlab_field_form_alter_defaults($form, $defaults);
  // Add the functionality that stores these settings upon submission.
  $form['#submit'][] = '_gitlab_field_form_user_register_profile_form_submit';
}

/**
 * Custom submit handler for user register/profile forms.
 */
function _gitlab_field_form_user_register_profile_form_submit($form, &$form_state) {
  // Save custom select field values to the DB.
  db_merge('gitlab_field_defaults')
    ->key(array('uid' => $form_state['values']['uid']))
    ->fields(array(
      'default_account' => $form_state['values']['gitlab_field_defaults']['default_account'],
      'default_project' => $form_state['values']['gitlab_field_defaults']['default_project'],
      'default_branch' => $form_state['values']['gitlab_field_defaults']['default_branch'],
    ))
    ->execute();
}

/**
 * Implements hook_user_delete().
 */
function gitlab_field_user_delete($account){
  db_delete('gitlab_field_defaults')
    ->condition('uid', $account->uid)
    ->execute();
}
