/**
 * @file
 * Custom scripts for GitLab Field feature.
 */

(function($) {

  // Make sure objects are exist.
  Drupal.tableDrag = Drupal.tableDrag || {};

  Drupal.behaviors.gitLabFileFieldWidget = {
    attach: function (context, settings) {

      // Table drag modifications.
      for (var key in Drupal.tableDrag) {
        if (Drupal.tableDrag.hasOwnProperty(key)) {
          if ($(Drupal.tableDrag[key].table).hasClass('gitlab_field_widget_linked_files_table')) {
            // Custom dropRow function behavior.
            Drupal.tableDrag[key].dropRow = function(event, self) {
              // Show own custom warning message.
              if ((self.rowObject != null) && (self.rowObject.changed == true)) {
                var $changesWarning = $('#' + $(self.table).attr('data-changes-warning-id'));
                if (!$changesWarning.hasClass('visible')) {
                  $changesWarning.hide().fadeIn('slow', function(){
                    $(this).addClass('visible');
                  });
                }
              }
              // Save original warning message related theme functions.
              var tableDragChangedMarker = Drupal.theme.tableDragChangedMarker;
              var tableDragChangedWarning = Drupal.theme.tableDragChangedWarning;
              // Override warning message related theme functions.
              Drupal.theme.tableDragChangedMarker = function() { return ''; };
              Drupal.theme.tableDragChangedWarning = function() { return ''; };
              // Call the original function.
              Drupal.tableDrag.prototype.dropRow(event, self);
              // Restore original warning message related theme functions.
              Drupal.theme.tableDragChangedMarker = tableDragChangedMarker;
              Drupal.theme.tableDragChangedWarning = tableDragChangedWarning;
            };
          }
        }
      }

      // Show changes warning message when a display checkbox changes.
      $('.gitlab-file-field-widget input[type="checkbox"].display').change(function(){
        var $table = $(this).closest('table[data-changes-warning-id]');
        if ($table.length) {
          var $changesWarning = $('#' + $table.attr('data-changes-warning-id'));
          if (!$changesWarning.hasClass('visible')) {
            $changesWarning.hide().fadeIn('slow', function(){
              $(this).addClass('visible');
            });
          }
        }
      });

      // Reset select elements' value after an AJAX response if needed.
      $('.gitlab-file-field-widget div.file-sources select[data-reset-value]').each(function(){
        // Get reference to the event handlers attached to this element.
        var eventHandlers = $(this).data('events');
        // Save the change event handlers attached to this element.
        var changeEventHandlers = eventHandlers['change'];
        // Clear all change event handlers to prevent AJAX requests.
        eventHandlers['change'] = [];
        // Reset element's value.
        $(this).val($(this).attr('data-reset-value')).change();
        // Restore change event handlers on this element.
        eventHandlers['change'] = changeEventHandlers;
      });

    }
  };

})(jQuery);
