<?php

/**
 * @file
 * GitLab field specific file entity class.
 */

/**
 * GitLab field specific file entity class.
 */
class GitLabFieldFileEntity extends Entity {

  /**
   * Get the file's path inside the project repository including the file name.
   */
  public function getPath() {
    return empty($this->folder_path) ? $this->name : $this->folder_path . '/' . $this->name;
  }

  /**
   * Returns with the web accessible raw URL of the file.
   */
  public function getRawURL() {
    return $this->project_url . '/raw/' . $this->branch_id . '/' . $this->getPath();
  }

  /**
   * Returns with the web accessible blob URL of the file.
   */
  public function getBlobURL() {
    return $this->project_url . '/blob/' . $this->branch_id . '/' . $this->getPath();
  }

  /**
   * Helper function to fetch the file's content from GitLab.
   */
  public function fetchContent() {
    // Construct file data.
    $file_data = array(
      'file_path' => $this->getPath(),
      'ref' => $this->branch_id,
    );
    // Load the account entity the file belongs to.
    $account = gitlab_accounts_load_account(NULL, array(
      'endpoint_url' => $this->endpoint_url,
      'remote_id' => $this->account_id,
    ));
    // Try to get the file content.
    if (!empty($account)) {
      $file_info = gitlab_repositories_get_file($file_data, $this->project_id, $account->getPrivateToken(), array(
        'wsdata' => array(
          'endpoint' => $account->endpoint_url,
        ),
      ));

      if (!empty($file_info['data'])) {
        $this->content = $file_info['data']['content'];
        $this->save();
      }
    }
  }

}
